<?php


/**
 * Menu callback.
 */
function user_board_admin_settings(&$form_state, $type) {
  $form_state['type'] = $type;
  
  $settings = user_board_get_settings($type);
  //print_r($settings);
  
  $form['user_board'] = array(
    '#type' => 'fieldset',
    '#title' => t('user_board'),
    '#collapsible' => FALSE,
    '#weight' => 0,
  );
  $form['user_board']['privateuser_board'] = array(
      '#type' => 'checkbox',
      '#title' => t('is Private user_board?'),
      '#default_value' => $settings['privateuser_board'],
      '#description' => t('if private, the entries can only be viewed by the user_board owner, user_board author and the person who has administer access.'),
  );
  $form['user_board']['hidetitle'] = array(
      '#type' => 'checkbox',
      '#title' => t('hide the title field'),
      '#default_value' => $settings['hidetitle'],
      '#description' => t('hides the title input field. applies to both node & comment form.'),
  );
  
  $form['user_board']['hideinputformat'] = array(
      '#type' => 'checkbox',
      '#title' => t('hide input format options'),
      '#default_value' => $settings['hideinputformat'],
  );
  $form['user_board']['hideauthor'] = array(
      '#type' => 'checkbox',
      '#title' => t('hide the author input field?'),
      '#default_value' => $settings['hideauthor'],
  );
	$form['user_board']['hidemenu'] = array(
      '#type' => 'checkbox',
      '#title' => t('hide the menu settings field?'),
      '#default_value' => $settings['hidemenu'],
  );
  $form['user_board']['hiderevision'] = array(
      '#type' => 'checkbox',
      '#title' => t('hide the node revision section?'),
      '#default_value' => $settings['hiderevision'],
  );
  $form['user_board']['hideoptions'] = array(
      '#type' => 'checkbox',
      '#title' => t('hide the extra options section?'),
      '#default_value' => $settings['hideoptions'],
  );
  $form['user_board']['view'] = array(
      '#type' => 'select',
      '#title' => t('Display Type'),
      '#default_value' => $settings['view'],
      '#options' => array('default'=>'default'),
  );
  $form['user_board']['viewmax'] = array(
      '#type' => 'select',
      '#title' => t('maximum entries per page'),
      '#default_value' => $settings['viewmax'],
      '#options' => array(5 => 5, 10 =>10, 15=>15, 20=>20, 25=>25, 50=>50),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 10,
  );
  return $form;
}