<?php


/**
 * Generate a block containing a node entry form.
 */
function user_board_user_page($account, $type) {
  global $user;
	
  $stypes = user_board_get_types();
  if (!$stypes[$type]){
    return;
  }
	
  $typename = $stypes[$type]->name;
  $output = '';
  
  if (node_access('create', $type)) {
    // Include page handler for node_add()
    module_load_include('inc', 'node', 'node.pages');
    // Note title before rendering of form.
    //$title = drupal_get_title();
    $form = node_add($type);
    $types = node_get_types('names');
    //Restore title, which will have been overridden.
    //drupal_set_title($title);
    
    $output .= '<h2>'.t('Post new !type', array('!type' => $typename)).'</h2>' . $form;
  }
  
  $settings = user_board_get_settings($type);
  
  if ($settings['view'] == 'default'){
    $sql = db_rewrite_sql("SELECT n.nid, n.sticky, n.created FROM {node} n LEFT JOIN {user_board} s ON n.nid = s.nid WHERE n.status = 1 AND s.uid = %d AND n.type = '%s' ORDER BY n.sticky DESC, n.created DESC");
    $sql_count = db_rewrite_sql("SELECT count(n.nid) FROM {node} n LEFT JOIN {user_board} s ON n.nid = s.nid WHERE n.status = 1 AND s.uid = %d AND n.type = '%s'");
    
    $result = pager_query($sql, $settings['viewmax'], 0, $sql_count, $account->uid, check_plain($type));
    
    $entries = '';
    
    $num_rows = FALSE;
    while ($node = db_fetch_object($result)) {
      //$entries .= node_view(node_load($node->nid), TRUE, FALSE, TRUE);
      $entries .= node_show(node_load($node->nid), NULL);      
      $num_rows = TRUE;
    }

    if ($num_rows) {
      $entries .= theme('pager', NULL, $settings['viewmax']);
    }else{
      $entries .= t("Currently there is no !type entries posted yet.", array('!type' => $stypes[$type]->name));
    }
    drupal_set_title(t("!name's !type(!count)", array('!name' => $account->name, '!type' => $typename, '!count' => db_result(db_query($sql_count, $account->uid, check_plain($type))))));
  }
  
  
  return    '<div class="user_board-post-form" >' . $output . '</div>' .
            '<h2 class="title user_board-content">' . t("!type entries", array('!type' => $typename)) . '</h2>'.
            '<div class="user_board-content" >' . $entries . '</div>';
}